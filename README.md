# Xebia tondeuse

## Architecture technique

```text
Xebia-tondeuse
├─┬ backend     → Module maven backend avec Spring Boot
│ ├── src
│ └── pom.xml
├─┬ frontend    → Module maven frontend avec Vue.js
│ ├── src
│ └── pom.xml
└── pom.xml     → Pom parent maven
```

## Technologies utilisées

 - Spring-boot 2.0.1
 - Vuejs 2.5.2
 - Docker 18.03.1-ce
 - Heroku 7.0.98
 - Maven 3.5.3
 - npm 6.1.0
 - Java/JavaEE 8
 - ES6
 - Webpack 3.6.0
 - tomcat 8.5.29
 
## Builder le projet

Sans docker :
```bash
mvn clean install
```

Avec docker :
```bash
./mvn clean install
```

## Lancer le mode développement

### Sans docker
Lancez le serveur `tomcat` sur `localhost:8080`
```bash
mvn --projects backend spring-boot:run
```

Puis lancez le server `webpack-dev-server` sur `localhost:80`
```bash
npm run dev
```

Accédez à l'application : [localhost:80](localhost:80)

### Avec docker

Lancez les serveurs avec la commande suivante :
```bash
docker-compose up
```

Accédez à l'application : [localhost:80](localhost:80)

## Lancer les tests

Sans docker
```bash
mvn test
```

Avec docker
```bash
./mvn test
```

## Déployer l'application avec `heroku`

Si vous possedez docker, vous pouvez utiliser la commande suivante : `./heroku`

Connectez vous avec le client `heroku`
```bash
heroku login
```

Créez le dépot
```bash
heroku create
```

Pushez avec git sur votre espace heroku
```bash
git push heroku master
```

Activez une instance web
```bash
heroku ps:scale web=1
```
