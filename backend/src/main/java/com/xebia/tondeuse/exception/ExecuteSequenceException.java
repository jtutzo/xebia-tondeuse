package com.xebia.tondeuse.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ExecuteSequenceException extends RuntimeException {

    /**
     * Code source de l'exception
     */
    @Getter
    private String source;

    public ExecuteSequenceException(String source) {
        super("Erreur lors de la lecture des données en entrée. Veuillez verifier que celle-ci soit cohérente.");
        this.source = source;
    }
}
