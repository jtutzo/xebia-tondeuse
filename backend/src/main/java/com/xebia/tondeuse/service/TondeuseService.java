package com.xebia.tondeuse.service;

import com.xebia.tondeuse.model.Pelouse;
import com.xebia.tondeuse.model.Tondeuse;

public interface TondeuseService {

    /**
     * Initialise une tondeuse avant l'exécution d'une séquence
     * @param tondeuse
     */
    void initTondeuse(Tondeuse tondeuse);

    /**
     * Exécute les instructions d'une tondeuse dans une pelouse
     * @param tondeuse
     * @param pelouse
     */
    void executeTondeuseInPelouse(Tondeuse tondeuse, Pelouse pelouse);

}
