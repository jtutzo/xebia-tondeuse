package com.xebia.tondeuse.service;

import com.xebia.tondeuse.model.Pelouse;
import com.xebia.tondeuse.model.Position;

public interface PelouseService {

    /**
     * Initialise une pelouse avant l'exécution d'une séquence
     * @param pelouse
     */
    void initPelouse(Pelouse pelouse);

    /**
     * Vérifie qu'une position n'est pas en dehors de la pelouse
     * @param position
     * @param pelouse
     * @return Boolean
     */
    Boolean positionIsOutOfPelouse(Position position, Pelouse pelouse);
}
