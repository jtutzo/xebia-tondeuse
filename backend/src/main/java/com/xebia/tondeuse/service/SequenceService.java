package com.xebia.tondeuse.service;

import com.xebia.tondeuse.model.Sequence;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface SequenceService {

    /**
     * Crée une séqunce à partir d'un fichier
     * @param file
     * @return Sequence
     * @throws IOException
     */
    Sequence createSequences(MultipartFile file) throws IOException;

    /**
     * Crée une séquence à partir d'une liste de string
     * @param listLines
     * @return Sequence
     */
    Sequence createSequence(List<String> listLines);

    /**
     * Exécute une séquence
     * @param sequence
     */
    void executeSequence(Sequence sequence);

}
