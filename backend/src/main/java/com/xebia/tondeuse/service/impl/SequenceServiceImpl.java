package com.xebia.tondeuse.service.impl;

import com.xebia.tondeuse.model.Pelouse;
import com.xebia.tondeuse.model.Sequence;
import com.xebia.tondeuse.model.Tondeuse;
import com.xebia.tondeuse.service.PelouseService;
import com.xebia.tondeuse.service.SequenceService;
import com.xebia.tondeuse.service.TondeuseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service("sequenceService")
public class SequenceServiceImpl implements SequenceService {

    @Autowired
    private PelouseService pelouseService;

    @Autowired
    private TondeuseService tondeuseService;

    @Override
    public Sequence createSequences(MultipartFile file) throws IOException {
        Sequence sequence = null;
        try (Stream<String> stream = new BufferedReader(new InputStreamReader(file.getInputStream())).lines()) {
            List<String> listLines = new ArrayList<>();
            stream.forEach((String value) -> {
                if (!StringUtils.isEmpty(value)) {
                    listLines.add(value);
                }
            });
            sequence = this.createSequence(listLines);
        }
        return sequence;
    }

    @Override
    public Sequence createSequence(List<String> listLines) {
        Pelouse pelouse = new Pelouse(listLines.get(0));

        List<Tondeuse> listTondeuses = new ArrayList<>();
        listLines = listLines.subList(1, listLines.size());
        for (int i = 0; i < listLines.size() / 2; i++) {
            List<String> subList = listLines.subList(i*2, (i*2)+2);
            Tondeuse tondeuse = new Tondeuse(subList.get(0), subList.get(1));
            listTondeuses.add(tondeuse);
        }

        return new Sequence(pelouse, listTondeuses);
    }

    @Override
    public void executeSequence(Sequence sequence) {
        pelouseService.initPelouse(sequence.getPelouse());
        for (Tondeuse tondeuse : sequence.getListTondeuses()) {
            tondeuseService.initTondeuse(tondeuse);
            if (pelouseService.positionIsOutOfPelouse(tondeuse.getPosition(), sequence.getPelouse()))
                throw new RuntimeException("La position initiale de la tondeuse est en dehors de la pelouse.");
            tondeuseService.executeTondeuseInPelouse(tondeuse, sequence.getPelouse());
            tondeuse.setPositionFinale(tondeuse.toString());
        }
    }

}

