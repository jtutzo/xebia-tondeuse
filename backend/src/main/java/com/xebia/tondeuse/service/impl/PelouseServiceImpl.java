package com.xebia.tondeuse.service.impl;

import com.xebia.tondeuse.model.Pelouse;
import com.xebia.tondeuse.model.Position;
import com.xebia.tondeuse.service.PelouseService;
import org.springframework.stereotype.Service;

@Service("pelouseService")
public class PelouseServiceImpl implements PelouseService {

    @Override
    public void initPelouse(Pelouse pelouse) {
        String[] coinSuperieur = pelouse.getCoinSuperieur().split(" ");
        pelouse.getPositionCoinSuperieur().setX(Integer.parseInt(coinSuperieur[0]));
        pelouse.getPositionCoinSuperieur().setY(Integer.parseInt(coinSuperieur[1]));
    }

    @Override
    public Boolean positionIsOutOfPelouse(Position position, Pelouse pelouse) {
        if (position == null)
            throw new NullPointerException("position mustn't be null.");

        if (pelouse == null)
            throw  new NullPointerException("pelouse mustn't be null.");

        if (pelouse.getPositionCoinInferieur().getX() > position.getX()
                || pelouse.getPositionCoinInferieur().getY() > position.getY()
                || pelouse.getPositionCoinSuperieur().getX() < position.getX()
                || pelouse.getPositionCoinSuperieur().getY() < position.getY())
            return Boolean.TRUE;
        return Boolean.FALSE;
    }

}
