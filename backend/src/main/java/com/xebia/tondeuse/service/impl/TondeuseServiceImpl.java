package com.xebia.tondeuse.service.impl;

import com.xebia.tondeuse.enumeration.Instruction;
import com.xebia.tondeuse.enumeration.Orientation;
import com.xebia.tondeuse.model.Pelouse;
import com.xebia.tondeuse.model.Position;
import com.xebia.tondeuse.model.Tondeuse;
import com.xebia.tondeuse.service.PelouseService;
import com.xebia.tondeuse.service.TondeuseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("tondeuseService")
public class TondeuseServiceImpl implements TondeuseService {

    @Autowired
    private PelouseService pelouseService;

    @Override
    public void initTondeuse(Tondeuse tondeuse) {
        String positionInitial = tondeuse.getPositionInitiale();
        String instructions = tondeuse.getInstructions();

        String[] listPositions = positionInitial.split(" ");
        tondeuse.getPosition().setX(Integer.parseInt(listPositions[0]));
        tondeuse.getPosition().setY(Integer.parseInt(listPositions[1]));
        tondeuse.setOrientation(Orientation.getByCode(listPositions[2]));

        for (Character character : instructions.toCharArray()) {
            Instruction instruction = Instruction.getByCode(character.toString().toUpperCase());

            if (instruction == null)
                throw new NullPointerException();

            tondeuse.getListInstructions().add(instruction);
        }
    }

    @Override
    public void executeTondeuseInPelouse(Tondeuse tondeuse, Pelouse pelouse) {
        for (Instruction instruction : tondeuse.getListInstructions()) {
            switch (instruction) {
                case AVANCE: this.avance(tondeuse, pelouse);
                    break;
                case DROITE: this.changeOrientation(tondeuse, Instruction.DROITE.getValeur());
                    break;
                case GAUCHE: this.changeOrientation(tondeuse, Instruction.GAUCHE.getValeur());
                    break;
            }
        }
    }

    private void avance(Tondeuse tondeuse, Pelouse pelouse) {
        Position currentPosition = tondeuse.getPosition();
        Position newPosition = new Position(currentPosition.getX().intValue(), currentPosition.getY().intValue());
        switch (tondeuse.getOrientation()) {
            case EAST: newPosition.setX(currentPosition.getX()+Instruction.AVANCE.getValeur());
                break;
            case WEST: newPosition.setX(currentPosition.getX()-Instruction.AVANCE.getValeur());
                break;
            case NORTH: newPosition.setY(currentPosition.getY()+Instruction.AVANCE.getValeur());
                break;
            case SOUTH: newPosition.setY(currentPosition.getY()-Instruction.AVANCE.getValeur());
                break;
        }
        if (!pelouseService.positionIsOutOfPelouse(newPosition, pelouse))
            tondeuse.setPosition(newPosition);
    }

    private void changeOrientation(Tondeuse tondeuse, Integer valeur) {
        Orientation orientation = tondeuse.getOrientation();
        Integer nouvelleValeur = orientation.getValeur() + valeur;

        if (nouvelleValeur < Orientation.NORTH.getValeur())
            orientation = Orientation.WEST;
        else if (nouvelleValeur > Orientation.WEST.getValeur())
            orientation = Orientation.NORTH;
        else
            orientation = Orientation.getByValeur(nouvelleValeur);

        tondeuse.setOrientation(orientation);
    }


}
