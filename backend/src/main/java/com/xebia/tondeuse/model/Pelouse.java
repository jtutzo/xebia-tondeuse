package com.xebia.tondeuse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Classe représentant un pelouse
 * @author jtutzo
 */
public class Pelouse {

    /**
     * Position du coin inferieur de la pelouse
     */
    @Getter
    @Setter
    @NonNull
    @JsonIgnore
    private Position positionCoinInferieur;

    /**
     * Position de coin supérieur de la pelouse
     */
    @Getter
    @Setter
    @NonNull
    @JsonIgnore
    private Position positionCoinSuperieur;

    /**
     * Position de coin supérieur de la pelouse au format string pour le webservice
     */
    @Getter
    @Setter
    private String coinSuperieur;

    /**
     * Constructeur par défaut
     */
    public Pelouse() {
        this.positionCoinInferieur = new Position();
        this.positionCoinSuperieur = new Position();
    }

    /**
     * Constructeur pour initialiser le bean
     * @param coinSuperieur
     */
    public Pelouse(String coinSuperieur) {
        this();
        this.coinSuperieur = coinSuperieur;
    }

}
