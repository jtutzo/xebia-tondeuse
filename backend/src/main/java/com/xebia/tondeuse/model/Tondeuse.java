package com.xebia.tondeuse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xebia.tondeuse.enumeration.Instruction;
import com.xebia.tondeuse.enumeration.Orientation;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui représente une tondeuse
 * @author jtutzo
 */
public class Tondeuse {

    /**
     * Position de la tondeuse à un instant t
     */
    @Getter
    @Setter
    @NonNull
    @JsonIgnore
    private Position position;

    /**
     * Orientation de la tondeuse à un instant t
     */
    @Getter
    @Setter
    @NonNull
    @JsonIgnore
    private Orientation orientation;

    /**
     * Liste des instructions à exécuter sur une pelouse
     */
    @Getter
    @NonNull
    @JsonIgnore
    private List<Instruction> listInstructions;

    /**
     * Liste des instructions au format String pour le webservice
     */
    @Getter
    @Setter
    private String instructions;

    /**
     * Position initiale au format String
     */
    @Getter
    @Setter
    private String positionInitiale;

    /**
     * Position finale au format String
     */
    @Getter
    @Setter
    private String positionFinale;

    /**
     * Contructeur par défaut
     */
    public Tondeuse() {
        this.position = new Position();
        this.orientation = Orientation.NORTH;
        this.listInstructions = new ArrayList<>();
    }

    /**
     * Constructeur pour initialiser le bean
     * @param positionInitiale
     * @param instructions
     */
    public Tondeuse(String positionInitiale, String instructions) {
        this();
        this.positionInitiale = positionInitiale;
        this.instructions = instructions;
    }

    /**
     * Redéfinotion de la méthode toString
     * @return String
     */
    @Override
    public String toString() {
        return String.format("%s %s", this.position, this.orientation.getCode());
    }

}
