package com.xebia.tondeuse.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une séquence à exécuter
 * @author jtutzo
 */
public class Sequence {

    /**
     * Pelouse de la séquence
     */
    @Getter
    @NonNull
    private Pelouse pelouse;

    /**
     * Liste de tondesuses de la séquence
     */
    @Getter
    @NonNull
    @JsonProperty("tondeuses")
    private List<Tondeuse> listTondeuses;

    /**
     * Constructeur par défaut
     */
    public Sequence() {
        this.pelouse = new Pelouse();
        this.listTondeuses = new ArrayList<>();
    }

    /**
     * Contructeur pour initialiser le bean
     * @param pelouse
     * @param listTondeuses
     */
    public Sequence(Pelouse pelouse, List<Tondeuse> listTondeuses) {
        this.pelouse = pelouse;
        this.listTondeuses = listTondeuses;
    }

    /**
     * Retourne le résultat après l'exécution de la séquence
     * @return List of String
     */
    public List<String> getResult() {
        List<String> result = new ArrayList<>();
        for (Tondeuse tondeuse : this.listTondeuses)
            result.add(tondeuse.toString());
        return result;
    }

}
