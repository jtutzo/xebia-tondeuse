package com.xebia.tondeuse.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Classe représentant un position dans l'espace 2D
 * @author jtutzo
 */
public class Position {

    /**
     * Valeur de l'axe x (abiscisse)
     */
    @Setter
    @Getter
    @NonNull
    private Integer x;

    /**
     * Valeur de laxe y (ordonnée)
     */
    @Setter
    @Getter
    @NonNull
    private Integer y;

    /**
     * Constructeur par défaut
     */
    public Position() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Constructeur pour initialiser le bean
     * @param x
     * @param y
     */
    public Position(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Redéfinition de la méthode toString
     * @return String
     */
    @Override
    public String toString() {
        return String.format("%s %s", this.getX(), this.getY());
    }

}
