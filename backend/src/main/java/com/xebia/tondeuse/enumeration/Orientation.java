package com.xebia.tondeuse.enumeration;

import lombok.Getter;

public enum Orientation {
    NORTH("N", "North", 0),
    EAST("E", "East", 1),
    SOUTH("S", "South", 2),
    WEST("W", "West", 3);

    /**
     * Code de l'orientation
     */
    @Getter
    private String code;

    /**
     * Nom de l'orientation
     */
    @Getter
    private String name;

    /**
     * Valeur de l'orientation (dans un plan)
     */
    @Getter
    private Integer valeur;

    /**
     * Constructeur par défault
     * @param code
     * @param name
     * @param valeur
     */
    Orientation(String code, String name, Integer valeur) {
        this.code = code;
        this.name = name;
        this.valeur = valeur;
    }

    /**
     * Retourne l'orientation correspondant au code (retourne une valeur null si il n'y a pas de correspondance)
     * @param code
     * @return Orientation
     */
    public static Orientation getByCode(String code) {
        for (Orientation orientation : Orientation.values()) {
            if (orientation.getCode().equals(code)) return orientation;
        }
        return null;
    }

    /**
     * Retourne l'orientation correspondanr à la valeur (retourne une valeur null si il n'y a pas de correspondance)
     * @param valeur
     * @return Orientation
     */
    public static Orientation getByValeur(Integer valeur) {
        for (Orientation orientation : Orientation.values()) {
            if (orientation.getValeur() == valeur) return orientation;
        }
        return null;
    }

}
