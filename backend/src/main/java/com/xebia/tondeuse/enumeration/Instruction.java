package com.xebia.tondeuse.enumeration;

import lombok.Getter;

public enum Instruction {
    GAUCHE("G", "Gauche", -1),
    DROITE("D", "Droite", 1),
    AVANCE("A", "Avance", 1);

    /**
     * Code de l'instruction
     */
    @Getter
    private String code;

    /**
     * Nom de l'instruction
     */
    @Getter
    private String name;

    /**
     * Valeur de l'instruction (dans un plan)
     */
    @Getter
    private Integer valeur;

    /**
     * Contructeur par défault
     * @param code
     * @param name
     * @param valeur
     */
    Instruction(String code, String name, Integer valeur) {
        this.code = code;
        this.name = name;
        this.valeur = valeur;
    }

    /**
     * Retourne l'instruction correpondant au code (si aucune correpondance retourne une valeur null)
     * @param code
     * @return Instruction
     */
    public static Instruction getByCode(String code) {
        for (Instruction instruction : Instruction.values()) {
            if (instruction.getCode().equals(code)) return instruction;
        }
        return null;
    }

}
