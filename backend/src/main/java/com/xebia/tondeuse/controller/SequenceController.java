package com.xebia.tondeuse.controller;

import com.xebia.tondeuse.exception.ExecuteSequenceException;
import com.xebia.tondeuse.model.Sequence;
import com.xebia.tondeuse.service.SequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class SequenceController {

    @Autowired
    private SequenceService sequenceService;

    /**
     * Execute une séquence à partir d'un fichier envoyé
     * @param file
     * @return sequence
     * @throws ExecuteSequenceException
     */
    @PostMapping("/sequences/exec/by-upload")
    public Sequence executeSequence(@RequestParam("file")MultipartFile file) throws ExecuteSequenceException {
        Sequence sequence = null;
        try {
            sequence = sequenceService.createSequences(file);
            sequenceService.executeSequence(sequence);
        } catch (Exception e) {
            throw new ExecuteSequenceException(e.getMessage());
        }
        return sequence;
    }

    /**
     * Execute une sequence à partir d'une sequence json envoyée
     * @param sequence
     * @return sequence
     * @throws ExecuteSequenceException
     */
    @PostMapping("/sequences/exec/by-webservice")
    public Sequence executeSequence(@RequestBody Sequence sequence) throws ExecuteSequenceException {
        try {
            sequenceService.executeSequence(sequence);
        } catch (Exception e) {
            throw new ExecuteSequenceException(e.getMessage());
        }
        return sequence;
    }

}
