package com.xebia.tondeuse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebia.tondeuse.model.Sequence;
import com.xebia.tondeuse.service.SequenceService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TondeuseApplicationTests {

	@Autowired
	private SequenceService sequenceService;

	private static final List<String> SEQUENCE_INPUT = Arrays.asList(new String [] {"5 5","1 2 N","GAGAGAGAA","3 3 E","AADAADADDA"});

    private static final List<String> SEQUENCE_RESULT = Arrays.asList(new String [] {"1 3 N","5 1 E"});
    
    private static final String FILE_TEXT = "sequence.txt";

    private static final String FILE_JSON = "sequence.json";

	private MockMultipartFile mockMultipartFile;

	private Sequence sequenceJson;

	@Test
	public void contextLoads() {
		assertThat(sequenceService).isNotNull();
	}

	@Before
	public void beforeTest() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
		this.mockMultipartFile = new MockMultipartFile(FILE_TEXT,
				Files.readAllBytes(new File(classLoader.getResource(FILE_TEXT).getFile()).toPath()));

		byte[] jsonData = Files.readAllBytes(new File(classLoader.getResource(FILE_JSON).getFile()).toPath());
		ObjectMapper objectMapper = new ObjectMapper();
		this.sequenceJson = objectMapper.readValue(jsonData, Sequence.class);
	}

	@Test
	public void testSequenceByList() {
		Sequence sequence = sequenceService.createSequence(SEQUENCE_INPUT);
		sequenceService.executeSequence(sequence);
		Assert.assertEquals(SEQUENCE_RESULT, sequence.getResult());
	}

	@Test
	public void testSequenceByFile() throws IOException{
		Sequence sequence = sequenceService.createSequences(mockMultipartFile);
		sequenceService.executeSequence(sequence);
		Assert.assertEquals(SEQUENCE_RESULT, sequence.getResult());
	}

	@Test
	public void testSequenceBySequence() {
		sequenceService.executeSequence(sequenceJson);
		Assert.assertEquals(SEQUENCE_RESULT, sequenceJson.getResult());
	}
}
